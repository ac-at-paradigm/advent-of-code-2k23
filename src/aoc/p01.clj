(ns aoc.p01 
  (:require [clojure.string :as str]))

(defn words->digits [input]
  (-> input
      (str/replace #"one" "one1one")
      (str/replace #"two" "two2two")
      (str/replace #"three" "three3three")
      (str/replace #"four" "four4four")
      (str/replace #"five" "five5five")
      (str/replace #"six" "six6six")
      (str/replace #"seven" "seven7seven")
      (str/replace #"eight" "eight8eight")
      (str/replace #"nine" "nine9nine")))

(defn keep-numbers [input] (apply str (filter #(Character/isDigit %) input)))

;The newly-improved calibration document consists of lines of text; each line
;originally contained a specific calibration value that the Elves now need to
;recover. On each line, the calibration value can be found by combining the
;first digit and the last digit (in that order) to form a single two-digit
;number.
(defn p1 [input]
  (let [lines (str/split-lines input)
        digit-lines (map keep-numbers lines)
        first-digits (map first digit-lines)
        last-digits (map last digit-lines)
        pairs (map vector first-digits last-digits) 
        coordinates (map #(apply (comp read-string str) %) pairs)]
    (reduce + 0 coordinates)))

(defn p2 [input]
  (let [lines (str/split-lines input)
        ; Convert string numbers "one"-"nine" to characters
        fixed-lines (map words->digits lines)
        digit-lines (map keep-numbers fixed-lines)
        first-digits (map first digit-lines)
        last-digits (map last digit-lines)
        pairs (map vector first-digits last-digits) 
        coordinates (map #(apply (comp read-string str) %) pairs)]
    (reduce + 0 coordinates)))

(comment

  (def test "two1nine
            eightwothree
            abcone2threexyz
            xtwone3four
            4nineeightseven2
            zoneight234
            7pqrstsixteen")

  ; 54953
  (p1 (slurp "resources/01.txt"))

  ; 281
  (p2 test)

  ; 53868
  (p2 (slurp "resources/01.txt")))