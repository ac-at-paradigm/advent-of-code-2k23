(ns aoc.p02
  (:require [clojure.string :as str]))


(def test-input
  "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green")

; only 12 red cubes, 13 green cubes, and 14 blue cubes

(def limits {:red 12 :green 13 :blue 14})

(defn within-limits? [record]
  (every? (fn [[k v]] (<= v (get limits k)))
          record))

(defn parse-round [input]
  (let [pair-strings (str/split input #", ")
        pairs (map #(str/split % #" ") pair-strings)]
    (reduce (fn [m [num color]]
              (assoc m (keyword color) (Integer/parseInt num)))
            {} pairs)))

(defn line->game
  [line]
  (let [[id-string game-string] (str/split line #": ")
        id                      (Integer/parseInt (last (str/split id-string #"Game ")))
        round-strings           (str/split game-string #"; ")
        rounds                  (map parse-round round-strings)]
    {:id     id
     :rounds rounds}))

(defn parse-input [input]
  (let [lines (str/split-lines input)]
    (map line->game lines)))

(defn p1 [input]
  (let [games (parse-input input)]
    (reduce (fn [res game] (+ res (:id game)))
            0
            (filter (fn [game]
                      (not (some (comp not within-limits?) (:rounds game))))
                    games))))

(defn compute-power [game]
  (let [{red   :red
         blue  :blue
         green :green
         :or   {red   1
                blue  1
                green 1}} game]
    (* red blue green)))

(defn max-colors
  "Take a list of rounds from the game and reduce the round records to find the
  max colors accross rounds."
  [rounds]
  (reduce (fn [maxed round]
            (reduce (fn [acc [k v]]
                      (assoc acc k (max v (k acc))))
                    maxed
                    round))
          {:red   1
           :blue  1
           :green 1}
          rounds))

(defn p2
  "Instead of checking limits, compute the max across all rounds for each color
  in each game. Multiply the values of the resulting colors. Sum the results."
  [input]
  (let [games       (parse-input input)
        maxed-games (map #(max-colors (:rounds %)) games)]
    (reduce + 0 (map compute-power maxed-games))))

(comment
  (p1 test-input)
  (p1 (slurp "resources/02.txt"))

  (p2 test-input)
  (p2 (slurp "resources/02.txt"))

  ; P1
  ; For each game,
  ;   For each round,
  ;     Compare record value against limit value
  ;       If greater, remove game
  
  ; Filter on games, predicate any round that fails comparison
  
  ; Compare round to limit
  ; Any round that fails compare
  ; Filter games
  
  ; P2
  ; Instead of filtering games, we reduce rounds to a single max record, compute
  ; power, and sum the results.

  (-> (parse-input test-input) first :rounds first compute-power)
  (-> (parse-input test-input) first :rounds max-colors)

  (let [rounds (-> (parse-input test-input) first :rounds)
        maxed  {:red   1
                :blue  1
                :green 1}]
    (reduce
     (fn [res round] (assoc res :red
                            (max (:red res) (:red round))))
     maxed
     rounds))

  ; Can't destrucure let-style in fn forms?
  (let [map {:a 42
             :b 1337}]
    ((fn [{a :a
           b :b} m] (+ a b)) map))


  (def invalid-game
    "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red")

  (let [line  (-> invalid-game str/split-lines first)
        game  (line->game line)
        round (first game)
        games (parse-input test-input)]
    (filter (fn [g] (not (some (comp not within-limits?) g))) games))


  (let [line                    (-> invalid-game str/split-lines first)
        [id-string game-string] (str/split line #": ")
        id                      (Integer/parseInt (last (str/split id-string #"Game ")))
        round-strings           (str/split game-string #"; ")
        game                    (map parse-round round-strings)]
    [id-string id])

  (str/split "Game 14" #"Game ")
  )